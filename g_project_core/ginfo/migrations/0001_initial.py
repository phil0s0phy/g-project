# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ginfo',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('g_name', models.CharField(max_length=50)),
                ('g_desc', models.CharField(max_length=200)),
                ('g_image', models.CharField(max_length=200)),
                ('create_date', models.DateTimeField(verbose_name='date created')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
