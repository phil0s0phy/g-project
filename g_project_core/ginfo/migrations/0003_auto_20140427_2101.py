# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ginfo', '0002_auto_20140427_2050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ginfo',
            name='g_image',
            field=models.FileField(upload_to=''),
        ),
    ]
