# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ginfo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ginfo',
            name='g_image',
            field=models.FileField(upload_to='not_used', db_index=True),
        ),
    ]
