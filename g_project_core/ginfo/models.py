from django.db import models

# Create your models here.

class Ginfo(models.Model):
	g_name = models.CharField(max_length=50)
	g_desc = models.CharField(max_length=200)
	g_image = models.FileField(upload_to='documents/%Y/%m/%d')
	create_date = models.DateTimeField('date created')